package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String userId, String start, String end) {

		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			//日付のデフォルト値を設定
			Date date = new Date();

			if(!StringUtils.isBlank(start)) {
				start += " 00:00:00";
			}else {
				start = "2020-01-01 00:00:00";
			}
			if(!StringUtils.isBlank(end)) {
				end += " 23:59:59";
			}else {
				end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
			}

			Integer id = null;
			if(!StringUtils.isBlank(userId)) {
				id = Integer.parseInt(userId);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
			commit(connection);

			return messages;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
	//つぶやきの削除
	public void delete(int messageId) {

		Connection connection = null;

		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
	//つぶやきの編集画面表示
	public Message selectMsg(String messageId) {

		Connection connection = null;
		int id = Integer.parseInt(messageId);

		try {
			connection = getConnection();
			Message message = new MessageDao().selectMsg(connection, id);
			commit(connection);
			return message;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
	//つぶやきの編集更新
	public void update(Message editMessage) {

		Connection connection = null;

		try {
			connection = getConnection();
			new MessageDao().update(connection, editMessage);
			commit(connection);
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}
}
