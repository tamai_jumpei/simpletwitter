package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {

	//編集前の投稿を表示させる
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String messageId = request.getParameter("id");
		List<String> errorMessages = new ArrayList<String>();

		Message editMessage = null;

		//レビュー後修正（空・空白文字判定）
		if(!StringUtils.isBlank(messageId) && messageId.matches("^[0-9]+$")) {
			editMessage = new MessageService().selectMsg(messageId);
		}
		if(editMessage == null) {
			errorMessages.add("不正なパラメータが入力されました");
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("./").forward(request, response);
			return;
		}
		request.setAttribute("message", editMessage);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	//編集した投稿を更新させる
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String text = request.getParameter("text") ;
		List<String> errorMessages = new ArrayList<String>();

		if(!isValid(text, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("id")));
		message.setText(request.getParameter("text"));

		new MessageService().update(message);
		response.sendRedirect(" ./");
	}

	//異常系処理
	private boolean isValid(String text, List<String> errorMessages) {

		if(StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		}else if(140 < text.length()) {
			errorMessages.add("140文字以内で入力してください");
		}
		if(errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}