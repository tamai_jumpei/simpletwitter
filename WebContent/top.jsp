<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>簡易Twitter</title>
</head>
<body>
	<div class="main-contents">
		<!-- ヘッダー -->
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login.jsp">ログイン</a>
				<a href="signup.jsp">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="setting">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<!-- ログイン情報 -->
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name }" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.email }" />
				</div>
				<div class="description">
					<c:out value="${loginUser.description }" />
				</div>
			</div>
		</c:if>
		<!-- 絞り込みフォーム -->
		<div>
			<form action="./" method="get">
				<input type="date" name="start" value="${start }">～
				<input type="date" name="end" value="${end }">
				<input type="submit" value="絞込">
			</form>
		</div>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
					</c:forEach>
				</ul>
			</div>
		</c:if>
		<!-- メッセージ投稿欄 -->
		<div class="form-area">
			<c:if test="${ isShowMessageForm }">
				<form action="message" method="post">
					いま、どうしてる？<br />
					<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
					<br /> <input type="submit" value="つぶやく">（140文字まで）
				</form>
			</c:if>
		</div>
		<!-- メッセージを表示   -->
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
						<span class="account"><a href="./?user_id=<c:out value="${message.userId}"/> "><c:out value="${message.account}" /></a></span>
						<span class="name"><c:out value="${message.name}" /></span>
					</div>
					<div class="text">
						<pre style="margin: 0;"><c:out value="${message.text}" /></pre>
					</div>
					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<!-- つぶやきの編集 -->
					<c:if test="${message.userId == loginUser.id }">
						<div style="float:left; margin-right: 10px;">
							<form action="./edit" method="get">
								<input id="id" name="id" type="hidden" value="${message.id }" />
								<input type="submit" value="編集">
							</form>
						</div>
					</c:if>
					<!-- つぶやきの削除 -->
					<c:if test="${message.userId == loginUser.id }">
						<div>
							<form action="./deleteMessage" method="post">
								<input id="id" name="id" type="hidden" value="${message.id }" />
								<input type="submit" value="削除">
							</form>
						</div>
					</c:if>
					<!-- つぶやきの返信 -->
					<div class="comment">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.messageId }">
								<div>
									<span class="account">
										<c:out value="${comment.account}" />
									</span>
									<span class="name">
										<c:out value="${comment.name}" />
									</span>
								</div>
								<div class="text">
									<pre><c:out value="${comment.text}" /></pre>
								</div>
								<div class="date">
									<fmt:formatDate value="${comment.createdDate}"
										pattern="yyyy/MM/dd HH:mm:ss" />
								</div>
							</c:if>
						</c:forEach>
						<c:if test="${ isShowMessageForm }">
							<form action="comment" method="post">
								返信<br />
								<input name="messageId" value="${message.id }" type="hidden" >
								<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
								<br /> <input type="submit" value="返信">（140文字まで）
							</form>
						</c:if>
					</div>
				</div>
			</c:forEach>
		</div>

		<div class="copyright">Copyright(c)TamaiJumpei</div>
	</div>
</body>
</html>